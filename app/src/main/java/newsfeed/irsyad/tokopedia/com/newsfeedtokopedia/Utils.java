package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utils {
    public static String getDateParsing(String parser, String dateString) {
        SimpleDateFormat format = new SimpleDateFormat(Constant.DATE_FORMAT, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            Date date = format.parse(dateString);
            return (String) DateFormat.format(parser, date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
