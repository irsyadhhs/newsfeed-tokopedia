package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.Constant;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.R;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.activity.ArticleBySourceActivity;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.source.Source;

public class NewsSourceAdapter extends RecyclerView.Adapter<NewsSourceAdapter.NewsHolder> {

    Context context;
    ArrayList<Source> sources;

    public NewsSourceAdapter(Context context, ArrayList<Source> sources) {
        this.context = context;
        this.sources = sources;
    }

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_source, viewGroup, false);
        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder holder, int i) {
        final Source source = sources.get(i);
        holder.category.setText(source.getCategory());
        holder.name.setText(source.getName());
        holder.countryLang.setText(source.getCountry()+" - "+source.getLanguage());
        holder.description.setText(source.getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ArticleBySourceActivity.class);
                i.putExtra(Constant.EXTRA_SOURCE_ID, source.getId());
                i.putExtra(Constant.EXTRA_SOURCE_NAME, source.getName());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sources.size();
    }

    class NewsHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.country_lang)
        TextView countryLang;

        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
