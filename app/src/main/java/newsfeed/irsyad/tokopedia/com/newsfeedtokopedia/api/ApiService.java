package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api;

import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.article.ResponseArticle;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.source.ResponseSource;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    //news source
    @GET("sources")
    Call<ResponseSource> getSources(@Query("apiKey") String apiKey);

    //article by news source
    @GET("everything")
    Call<ResponseArticle> getArticleBySources(@Query("sources") String sources,
                                              @Query("page") int page,
                                              @Query("pageSize") int pageSize,
                                              @Query("apiKey") String apiKey);
    @GET("everything")
    Call<ResponseArticle> getArticleBySourcesPage(@Query("sources") String sources,
                                                  @Query("page") String page,
                                                  @Query("apiKey") String apiKey);

    //article by news source
    @GET("everything")
    Call<ResponseArticle> getArticleBySearch(@Query("q") String q,
                                             @Query("sources") String sources,
                                             @Query("page") int page,
                                             @Query("pageSize") int pageSize,
                                             @Query("apiKey") String apiKey);
}
