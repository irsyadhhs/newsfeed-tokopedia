package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia;

public class Constant {
    public static final String NEWS_API = "bd96e63f7bd34022a9fa72fdc419d889";
    public static final String BASE_URL = "https://newsapi.org/v2/";
    public static final String EXTRA_SOURCE_ID = "EXTRA_SOURCE_ID";
    public static final String EXTRA_SOURCE_NAME = "EXTRA_SOURCE_NAME";
    public static final String EXTRA_ARTICLE_URL = "EXTRA_ARTICLE_URL";
    public static final String EXTRA_ARTICLE_TITLE = "EXTRA_ARTICLE_TITLE";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_PARSER = "EEE, dd MMM yyyy HH:mm";
}
