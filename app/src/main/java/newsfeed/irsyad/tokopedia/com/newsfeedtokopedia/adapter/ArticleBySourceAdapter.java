package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.Constant;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.R;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.Utils;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.activity.ArticleViewActivity;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.article.Article;

public class ArticleBySourceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Article> articles;

    private Context context;
    private boolean isLoadingAdded = false;
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    public ArticleBySourceAdapter(Context context, ArrayList<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM:
                View v1 = inflater.inflate(R.layout.item_article_by_source, viewGroup, false);
                viewHolder = new ArticleHolder(v1);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingHolder(v2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Article article = articles.get(i);
        switch (getItemViewType(i)) {
            case ITEM:
                ArticleHolder articleHolder = (ArticleHolder) holder;
                articleHolder.title.setText(article.getTitle());
                articleHolder.description.setText(article.getDescription());
                articleHolder.author.setText(article.getAuthor());

                StringBuilder detail = new StringBuilder();
                detail.append("by ");
                detail.append(article.getSource().getName());
                detail.append(" | Published at ");
                detail.append(Utils.getDateParsing(Constant.DATE_PARSER, article.getPublishedAt()));

                articleHolder.detail.setText(detail);
                Picasso.get()
                        .load(article.getUrlToImage())
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .into(articleHolder.image);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, ArticleViewActivity.class);
                        i.putExtra(Constant.EXTRA_ARTICLE_URL, article.getUrl());
                        i.putExtra(Constant.EXTRA_ARTICLE_TITLE, article.getTitle());
                        context.startActivity(i);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public int getItemViewType ( int position){
        return (position == articles.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public Article getItem ( int position){
        return articles.get(position);
    }

    public void add (Article article){
        articles.add(article);
        notifyItemInserted(articles.size() - 1);
    }

    public void addLoadingFooter () {
        isLoadingAdded = true;
        add(new Article());
    }

    public void removeLoadingFooter () {
        isLoadingAdded = false;

        int position = articles.size() - 1;
        Article item = getItem(position);

        if (item != null) {
            articles.remove(position);
            notifyItemRemoved(position);
        }
    }

    class ArticleHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.author)
        TextView author;
        @BindView(R.id.detail)
        TextView detail;
        @BindView(R.id.urlToImage)
        ImageView image;

        public ArticleHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class LoadingHolder extends RecyclerView.ViewHolder {

        public LoadingHolder(View itemView) {
            super(itemView);
        }
    }
}
