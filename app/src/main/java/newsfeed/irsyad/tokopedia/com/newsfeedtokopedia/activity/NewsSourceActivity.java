package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.Constant;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.R;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.adapter.NewsSourceAdapter;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.ApiInterface;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.ApiService;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.source.ResponseSource;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.source.Source;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewsSourceActivity extends AppCompatActivity {

    @BindView(R.id.rv_newssource)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ArrayList<Source> sourceList;
    NewsSourceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_source);

        ButterKnife.bind(this);
        initRecyclerview();
    }

    private void initRecyclerview(){
        sourceList = new ArrayList<>();
        adapter = new NewsSourceAdapter(this, sourceList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.hasFixedSize();
        loadData();
    }

    private void loadData(){
        showProgress();
        Retrofit retrofit = ApiInterface.getInstance(Constant.BASE_URL).getRetrofit();
        ApiService load = retrofit.create(ApiService.class);

        Call<ResponseSource> getCall = load.getSources(Constant.NEWS_API);
        getCall.enqueue(new Callback<ResponseSource>() {
            @Override
            public void onResponse(Call<ResponseSource> call, Response<ResponseSource> response) {
                if(response.body()!=null) {
                    sourceList.addAll(response.body().getSources());
                    adapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(NewsSourceActivity.this, "there is nothing to display at the moment", Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseSource> call, Throwable t) {
                Toast.makeText(NewsSourceActivity.this, "could not retrieve source", Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress(){
        progressBar.setVisibility(View.GONE);
    }
}
