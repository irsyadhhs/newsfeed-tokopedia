package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.Constant;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.PaginationScrollListener;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.R;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.adapter.ArticleBySourceAdapter;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.ApiInterface;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.ApiService;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.article.Article;
import newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api.model.article.ResponseArticle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ArticleBySourceActivity extends AppCompatActivity {

    @BindView(R.id.rv_article)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    SearchView searchView;

    ArrayList<Article> articleList;
    ArrayList<Article> articleListBackup;
    ArticleBySourceAdapter adapter;
    String sourceId, sourceName;
    private boolean isSearch = false;
    private String mQuery;

    //pagination
    private static final int PAGE_START = 1;
    private static final int OFFSET = 5;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGE;
    private int currentPage = PAGE_START;
    private int currentPageSearch = PAGE_START;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_by_source);

        ButterKnife.bind(this);
        initVar();
        initRecyclerview();
    }

    private void initVar(){
        sourceId = getIntent().getStringExtra(Constant.EXTRA_SOURCE_ID);
        sourceName = getIntent().getStringExtra(Constant.EXTRA_SOURCE_NAME);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Article");
        getSupportActionBar().setSubtitle(sourceName);

        articleList = new ArrayList<>();
        articleListBackup = new ArrayList<>();
        adapter = new ArticleBySourceAdapter(this, articleList);
    }

    private void initRecyclerview(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.hasFixedSize();
        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                //if(!isLastPage) {
                isLoading = true;

                if(!isSearch) currentPage += 1;
                else currentPageSearch += 1;

                loadNextData();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGE;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        loadData();
    }

    private void loadData(){
        showProgress();
        Retrofit retrofit = ApiInterface.getInstance(Constant.BASE_URL).getRetrofit();
        ApiService load = retrofit.create(ApiService.class);

        Call<ResponseArticle> getCall;
        if(!isSearch) {
            getCall = load.getArticleBySources(sourceId, currentPage, OFFSET, Constant.NEWS_API);
        }else{
            getCall = load.getArticleBySearch(mQuery, sourceId, currentPageSearch, OFFSET, Constant.NEWS_API);
        }
        getCall.enqueue(new Callback<ResponseArticle>() {
            @Override
            public void onResponse(Call<ResponseArticle> call, Response<ResponseArticle> response) {
                if(response.body()!=null) {
                    if(response.body().getTotalResults() == 0){
                        Toast.makeText(ArticleBySourceActivity.this, "there is nothing to display at the moment", Toast.LENGTH_SHORT).show();
                    }
                    TOTAL_PAGE = response.body().getTotalResults() / OFFSET;
                    Log.d("article", "total page "+TOTAL_PAGE);
                    Log.d("article", "total result "+response.body().getTotalResults());
                    articleList.addAll(response.body().getArticles());
                    adapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(ArticleBySourceActivity.this, "there is nothing to display at the moment", Toast.LENGTH_SHORT).show();
                }

                isLoading = false;
                if (currentPage < TOTAL_PAGE) adapter.addLoadingFooter();
                else isLastPage = true;
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseArticle> call, Throwable t) {
                Toast.makeText(ArticleBySourceActivity.this, "could not retrieve source", Toast.LENGTH_SHORT).show();
                isLoading = false;
                hideProgress();
            }
        });
    }

    private void loadNextData(){
        Retrofit retrofit = ApiInterface.getInstance(Constant.BASE_URL).getRetrofit();
        ApiService load = retrofit.create(ApiService.class);

        Call<ResponseArticle> getCall;
        if(!isSearch) {
            getCall = load.getArticleBySources(sourceId, currentPage, OFFSET, Constant.NEWS_API);
        }else{
            getCall = load.getArticleBySearch(mQuery, sourceId, currentPageSearch, OFFSET, Constant.NEWS_API);
        }
        getCall.enqueue(new Callback<ResponseArticle>() {
            @Override
            public void onResponse(Call<ResponseArticle> call, Response<ResponseArticle> response) {
                if(response.body()!=null) {
                    adapter.removeLoadingFooter();  // 2
                    isLoading = false;

                    articleList.addAll(response.body().getArticles());
                    adapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(ArticleBySourceActivity.this, "there is nothing to display at the moment", Toast.LENGTH_SHORT).show();
                }
                if (currentPage != TOTAL_PAGE) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<ResponseArticle> call, Throwable t) {
                Toast.makeText(ArticleBySourceActivity.this, "could not retrieve source", Toast.LENGTH_SHORT).show();
                isLoading = false;
                if (currentPage != (TOTAL_PAGE - 1)) adapter.addLoadingFooter();
                else isLastPage = true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_search, menu);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();

        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                articleListBackup.addAll(articleList);
                isSearch = true;
                mQuery = query;
                loadData();

                articleList.clear();
                adapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(isSearch){
            articleList.clear();
            articleList.addAll(articleListBackup);
            adapter.notifyDataSetChanged();
            articleListBackup.clear();
            isSearch = false;
            //Toast.makeText(this, "in search", Toast.LENGTH_SHORT).show();
        }else{
            finish();
        }
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress(){
        progressBar.setVisibility(View.GONE);
    }
}
