package newsfeed.irsyad.tokopedia.com.newsfeedtokopedia.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiInterface {
    private static ApiInterface mApiClient;
    private Retrofit retrofit;

    public static ApiInterface getInstance(String base_url) {
        mApiClient = new ApiInterface(base_url);
        return mApiClient;
    }

    private ApiInterface(String base_url){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);

        Gson mGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(base_url)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(mGson));
        retrofit = builder.build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
